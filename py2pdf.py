#! /usr/bin/env python3

from shutil import copy, move, rmtree
from pathlib import Path
from tempfile import mkdtemp
from subprocess import check_call
from argparse import ArgumentParser

TEX_PREAMBLE = r"""
\documentclass[a4paper,11pt]{article}
\usepackage[margin=.75in]{geometry}
\usepackage{pdfpages}
\usepackage{minted}
%\pagenumbering{gobble}
\setminted{frame=leftline, fontsize=\small, breaklines}
\usepackage{pgfpages}
\pgfpagesuselayout{2 on 1}[a4paper,landscape]
\usepackage{intcalc}
\usepackage{ifthen}

% Color bottom page numbering white
\usepackage{xcolor,etoolbox}
\makeatletter 
\patchcmd{\ps@plain}{\thepage}{\textcolor{white}{\thepage}}{}{}
\makeatother

\begin{document}
\pagestyle{plain}
"""

def parse_args():
	parser = ArgumentParser("Turns a directory's content into a single pdf")
	parser.add_argument("-d", "--debug", help="Flag disabling temp directory deletion", action="store_true")
	parser.add_argument("-o", "--output", help="Path/Name given to the output pdf", default="./INF2.pdf")
	parser.add_argument("DIRECTORY", nargs="+", help="Paths to directories to convert")
	return parser.parse_args()

def process_file(path: Path, parent: Path, tempdir: Path) -> str:
	newpath = tempdir/(parent.name.replace(" ", "_") + "_" + path.name.replace(" ", "_"))
	copy(path, newpath)
	if ".odt" in path.suffix or ".docx" in path.suffix:
		check_call(["soffice", "--headless", "--invisible", "--nodefault", "--view", "--nolockcheck", "--nologo", "--norestore", "--nofirststartwizard", "--convert-to", "pdf", str(newpath)], cwd=tempdir)
		return process_file(tempdir/f"{newpath.stem}.pdf", parent, tempdir)
	elif ".pdf" in path.suffix:
		return r"\includepdf[pages=-]{" + str(newpath) + r"}"
	elif ".py" in path.suffix:
		return r"\paragraph{" + newpath.stem.replace("_", "\_") + r"}\inputminted{python}{" + str(newpath) + r"}"
	elif ".png" in path.suffix or ".jpg" in path.suffix:
		return r"\includegraphics[width=\linewidth]{" + str(newpath) + r"}"
	elif ".sqlite" in path.suffix or ".xlsx" in path.suffix:
		return ""
	else:
		raise ValueError(f"Unknown extension {path.suffix}")

def main():
	args = parse_args()
	OUTPUT = Path(args.output).expanduser().resolve()

	tempdir = Path(mkdtemp())
	print(tempdir)
	output_tex = TEX_PREAMBLE
	for directory in args.DIRECTORY:
		directory = Path(directory).expanduser().resolve(strict=True)
		if not directory.is_dir(): continue
		files = sorted(directory.iterdir())
		nonpy_files = [f for f in files if f.suffix!=".py"]
		py_files = [f for f in files if f.suffix==".py"]
		# Place python files at the end
		files = nonpy_files + py_files
		if len(nonpy_files)==0:
			output_tex += r"\section{" + directory.stem + "}\n"
		for path in files:
			output_tex += process_file(path, directory, tempdir) + "\n"
		output_tex += r"""
% Clear 2-sided page
\clearpage
\whiledo{\NOT \intcalcMod{\value{page}}{4}=1} {
    ~\newpage
}
"""

	output_tex += "\end{document}"

	with open(tempdir/"main.tex", "w") as f:
		f.write(output_tex)

	check_call(["latexmk", "-shell-escape", "-pdf", "-interaction=nonstopmode", "-xelatex"], cwd=tempdir)
	move(tempdir/"main.pdf", OUTPUT)
	rmtree(tempdir)

if __name__=="__main__":
	main()
