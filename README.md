# Py2PDF

Python script to export python files and accompanying documents from multiple folders into a single PDF.

## Usage

```bash
py2pdf [-h] [-d] [-o OUTPUT] DIRECTORY [DIRECTORY ...]
```
with:

- DIRECTORY Paths to directories to convert
- `h` or `--help` to show help
- `d` or `--debug` to disable temp directory deletion
- `o OUTPUT` or `--output OUTPUT` to give a path/name given to the output pdf
